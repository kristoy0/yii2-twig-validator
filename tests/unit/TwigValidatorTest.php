<?php

namespace tests\unit;

use Codeception\Test\Unit;
use kristoy0\twigvalidator\EmptyLoader;
use kristoy0\twigvalidator\TwigValidator;
use Twig\Environment;

class TwigValidatorTest extends Unit
{
    public function testValidValue()
    {
        $validator = new TwigValidator();
        $value = '{% set foo = {
            bar: foo,
            baz: foobaz
        } %}';

        $this->assertTrue($validator->validate($value));
    }

    public function testInvalidValue()
    {
        $validator = new TwigValidator();
        $value = '{% set foo = {
            bar: foo
            baz: foobaz
        } %}';

        $this->assertFalse($validator->validate($value));
    }

    public function testCustomTwigInstance()
    {
        $loader = new EmptyLoader();

        $validator = new TwigValidator([
            'twig' => new Environment($loader),
        ]);

        $value = '{% set foo = {
            bar: foo,
            baz: foobaz
        } %}';

        $this->assertTrue($validator->validate($value));
    }
}
