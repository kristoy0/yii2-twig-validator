<?php

namespace tests\unit;

use Codeception\Test\Unit;
use kristoy0\twigvalidator\EmptyLoader;
use Twig\Error\LoaderError;

class EmptyLoaderTest extends Unit
{
    public function testGetSourceContextThrows()
    {
        $loader = new EmptyLoader();

        $this->expectException(LoaderError::class);
        $loader->getSourceContext('');
    }

    public function testGetCacheKeyThrows()
    {
        $loader = new EmptyLoader();

        $this->expectException(LoaderError::class);
        $loader->getCacheKey('');
    }

    public function testIsFreshThrows()
    {
        $loader = new EmptyLoader();

        $this->expectException(LoaderError::class);
        $loader->isFresh('', 0);
    }

    public function testDoesntExist()
    {
        $loader = new EmptyLoader();
        $result = $loader->exists('');

        $this->assertFalse($result);
    }
}
