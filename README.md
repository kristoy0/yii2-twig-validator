# Twig validator for Yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist kristoy0/yii2-twig-validator
```

or add

```json
"kristoy0/yii2-twig-validator": "~1.0.0"
```

to the `require` section of your composer.json.
