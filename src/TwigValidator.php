<?php

namespace kristoy0\twigvalidator;

use Twig\Environment;
use Twig\Error\SyntaxError;
use Twig\Source;
use yii\validators\Validator;

class TwigValidator extends Validator
{
    /**
     * Twig instance
     *
     * @var Environment
     */
    private $_twig;

    /**
     * @return Environment
     */
    private function getTwig()
    {
        if (!$this->_twig) {
            $loader = new EmptyLoader();
            $this->_twig = new Environment($loader);
        }

        return $this->_twig;
    }

    /**
     * @param Environment $twig
     *
     * @return void
     */
    public function setTwig(Environment $twig)
    {
        $this->_twig = $twig;
    }

    /**
     * @inheritDoc
     */
    public function validateValue($value)
    {
        try {
            $this->getTwig()->parse(
                $this->getTwig()->tokenize(new Source($value, get_class($this)))
            );

            return null;
        } catch (SyntaxError $err) {
            return [$err->getMessage(), []];
        }
    }
}
