<?php

namespace kristoy0\twigvalidator;

use Twig\Error\LoaderError;
use Twig\Loader\LoaderInterface;
use Twig\Source;

class EmptyLoader implements LoaderInterface
{
    /**
     * @inheritDoc
     */
    public function getSourceContext(string $name): Source
    {
        throw new LoaderError(get_class($this) . ' cannot be used for rendering.');
    }

    /**
     * @inheritDoc
     */
    public function getCacheKey(string $name): string
    {
        throw new LoaderError(get_class($this) . ' cannot be used for rendering.');
    }

    /**
     * @inheritDoc
     */
    public function isFresh(string $name, int $time): bool
    {
        throw new LoaderError(get_class($this) . ' cannot be used for rendering.');
    }

    /**
     * @inheritDoc
     */
    public function exists(string $name)
    {
        return false;
    }
}
